<?php
// $Id: action_view_profile.inc

/**
 * This include file implements action_views functionality on behalf of profile.module
 */

// permissions
$action_view_permissions[] = 'multiple edit of user profiles';

/**
 * Invoke hook_views_operations() for allow edit any profile fields
 *
 * @return array
 */
function profile_views_operations($nodes = NULL)
{
	global $form_values;
	$operations = array();
	

	if (user_access('multiple edit of user profiles')) {
		if (!action_view_check_node_type($nodes, 'usernode')){
			return $operation;
		}

		$profile_form = array();
		foreach (profile_categories() as $category)
		{
			$profile_form = array_merge($profile_form, profile_form_profile(array(), user_load(array('uid' => 0)), $category['name']));
		}
		$profile_fields = array();
		foreach (profile_views_get_fields() as $field)
		{
			$profile_fields[$field->name] = array(
			'title' => t($field->title),
			'form' => array(
			$field->name => $profile_form[$field->category][$field->name]
			),
			'category' => $field->category
			);
		}

		$edit_fields = array();
		foreach ($profile_fields as $key => $value)
		{
			$edit_fields['edit_profile_field-'. $key] = $value['title'];
		}

		if (count($profile_fields))
		{
			$edit_field_operations = array(
						t('Edit profile field for the selected users') => array(
										'label' => $edit_fields,
										),
			);
			foreach ($profile_fields as $key => $value)
			{
				$edit_field_operations['form_fields_'.$key] = $value;
			}
			$operations = $edit_field_operations;
		}


	}
	// If the form has been posted, we need to insert the proper data for field editing if necessary.
	if ($form_values)
	{
		foreach ($form_values['operations'] as $op)
		{
			$operation_field = explode('-', $op);
			$operation = $operation_field[0];
			$field = $operation_field[1];
			if ($operation == 'edit_profile_field')
			{
				$category = $edit_field_operations['form_fields_'.$field]['category'];
				if (user_access('administer access control'))
				{
					$operations[$op] = array(
					'callback' => 'edit_profile_field',
					'callback arguments' => array($form_values, $operation, $field, $category),
					);
				}
				else
				{
					watchdog('security', t('Detected malicious attempt to alter protected node fields.'), WATCHDOG_WARNING);
					return;
				}
			}
		}
	}
	return  $operations;
}

/**
 * Callback functions for save changes of profile fields
 *
 * @param array $nids
 * @param array $form_values
 * @param string $operation
 * @param string $field
 * @param string $category
 */
function edit_profile_field($nids, $form_values, $operation, $field, $category)
{
	$fields = profile_views_get_fields();
	
	if ($operation == 'edit_profile_field')
	{
		foreach ($nids as $key => $nid)
		{
			$node = node_load(array('nid' => $nid));
			$user = $node->user;
			foreach ($fields as $f)
			{
				$f = $f->name;
				$edit_fields[$f] = $user->$f;
			}
			$edit_fields[$field] = $form_values[$field];
			profile_save_profile($edit_fields, $user, $category);
		}
	}
}