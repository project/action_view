<?php
// $Id: actionview_node.inc

/**
 * This include file implements actionviews functionality on behalf of node.module
 */

//permissions
$actionview_permissions[] = 'multiple edit of node titles and bodies';

/**
 * Invoke hook_views_operations() for allow standard node operations and edit of title and body
 *
 * @return array
 */
function node_views_operations($nodes = NULL)
{
	global $form_values;
	$operations = array();
	if (actionview_check_node_type($nodes, 'usernode'))
	{
		return $operations;
	}
	if (user_access('multiple edit of node titles and bodies')) 
	{
		


		$node_fields = array(
		'title' => array(
			'title' => t('Edit Title'),
			'form' => array(
						'title' => array(
										'#type' => 'textfield',
										'#title' => t('Node title'),
										'#size' => 60
										)
						)
			),
		'body' => array(
						'title' => t('Edit Body'),
						'form' => array(
									'body' => array(
												'#type' => 'textarea',
												'#title' => t('Node body'),
												'#rows' => 4,
												'#cols' => 120,
												'#weight' => 20,
												)
									)
						)
		);
		if (count($node_fields))
		{
			foreach ($node_fields as $key => $value)
			{
				$edit_field_operations['edit_node_field-'. $key] = array('label' => $value['title']);
				$edit_field_operations['form_fields_'.$key] = $value;
			}
			$operations = $edit_field_operations;
		}
	}
	if (user_access('administer nodes'))
	{
		$operations = array_merge(module_invoke_all('node_operations', $nodes), $operations);
		$operations['delete']['callback'] = 'actionview_node_delete';
	}
	return  $operations;
}

function actionview_node_delete($nodes)
{
	foreach ($nodes as $node)
	{
		node_delete($node);
	}
}

