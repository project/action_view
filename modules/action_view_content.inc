<?php
// $Id: action_view_content.inc

/**
 * This include file implements action_views functionality on behalf of content.module
 */
//permissions
$action_view_permissions[] = 'multiple edit of cck fields';

/**
 * Invoke hook_views_operations() for allow edit fields added by cck
 *
 * @return array
 */
function content_views_operations($nodes = NULL)
{
	global $form_values;
	$operations = array();
	
	if (user_access('multiple edit of cck fields')) {
		// get fields only for this node type
		$n = reset($nodes);
		$node  = node_load(array('nid' => $n->nid));
		if (!action_view_check_node_type($nodes, $node->type))
		{
			return $operations;
		}
		$content_types = content_types($node->type);
		$cck_fields = $content_types['fields'];
		
		// form fields for each node field 
		$form = content_form($node);
		if (count($cck_fields))
		{

			foreach ($cck_fields as $key => $value)
			{
				$value['form'][$key] = $form[$key];
				$edit_field_operations['edit_node_field-'.$key] = array('label' => t('Edit ').$value['widget']['label']);
				$edit_field_operations['form_fields_'.$key] = $value;
				
			}
			$operations = $edit_field_operations;
		}
	}
	return  $operations;
}

/**
 * Removes not edited cck fields from nodes for avoiding changes in this fields
 *
 * @param string $form_id
 * @param array $form_values
 */
function  content_views_operations_validate($form_id, &$form_values)
{
			$node = reset($form_values['nodes']);
			$content_types = content_types($node->type);
			foreach ($content_types['fields'] as $f)
			{
				if (!isset($form_values[$f['field_name']]))
				{
					foreach ($form_values['nodes'] as $key => $node)
					{
						unset($form_values['nodes'][$key]->$f['field_name']);
					}
				}
			}
}