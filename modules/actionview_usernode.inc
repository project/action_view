<?php
// $Id: actionview_node.inc

/**
 * This include file implements actionviews functionality on behalf of usernode.module
 */


/**
 * Invoke hook_views_operations() for allow standard user operations
 *
 * @return array
 */
function usernode_views_operations($nodes = array())
{
	global $form_values;
	if (user_access('administer users') && actionview_check_node_type($nodes, 'usernode'))
	{
		$operations = module_invoke_all('user_operations');
		$operations['delete']['callback'] = 'actionview_user_delete';
		// add uid for callbacks form user module
		if ($form_values['step'] > 2)
		{
			foreach ($operations as $key => $value)
			{
				$operations[$key]['id_field'] = 'accounts';
			}
			foreach ($form_values['nids'] as $nid)
			{
				$node = node_load(array('nid' => $nid));
				$form_values['accounts'][] = $node->user->uid;
			}
		}
	}
	else 
	{
		$operations = array();	
	}
	return  $operations;
}


function actionview_user_delete($acounts)
{
	if (user_access('administer users'))
	{
		foreach ($acounts as $uid)
		{
			user_delete(array(), $uid);
		}
	}
}